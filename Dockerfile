FROM debian:buster

RUN apt-get -qq update
RUN apt-get install -y x11vnc xvfb xdotool libxcursor1 libegl1 bzip2 libnss3 libegl1-mesa x11-xkb-utils libasound2 libpci3 libxslt1.1 libxkbcommon0 libxss1 libxcomposite1
RUN apt-get install -y pulseaudio wget expect telnet screen sqlite3 dos2unix curl gnupg2

#install teamspeak
RUN wget -O /teamspeak_dl.run https://files.teamspeak-services.com/releases/client/3.5.2/TeamSpeak3-Client-linux_amd64-3.5.2.run
RUN chmod +x /teamspeak_dl.run
RUN yes y | /teamspeak_dl.run
RUN rm /teamspeak_dl.run

#install spotify
RUN curl -sS https://download.spotify.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb http://repository.spotify.com stable non-free" | tee /etc/apt/sources.list.d/spotify.list
RUN apt-get -qq update
RUN apt-get install -y spotify-client

RUN useradd -ms /bin/bash ts3 
RUN chown -R ts3:ts3 /TeamSpeak3-Client-linux_amd64/

COPY scripts /scripts

EXPOSE 5900
VOLUME ["/home/ts3/.ts3client"]

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]