#!/bin/sh

if [ -z ${spotify_user+x} ]; then
    echo "spotify_user is not set"
    exit 1
fi
if [ -z ${spotify_pass+x} ]; then
    echo "spotify_pass is not set"
    exit 1
fi
if [ -z ${ts3_address+x} ]; then
    echo "ts3_address is not set"
    exit 1
fi

#fix volume permissions
chown -R ts3:ts3 /home/ts3/.ts3client

echo "setting up pulseaudio"
su -c "sh /scripts/pulseaudio.sh" - ts3

echo "setting up virtual display"
screen -dmS xvfb Xvfb :99 -screen 0 1280x1024x24 -ac -nolisten tcp
sleep 3 #wait for startup

echo "setting up teamspeak"
su -c "sh /scripts/ts3setup.sh SpotifyBot" - ts3

echo "setting up spotify"
su -c "sh /scripts/spotify_setup.sh $spotify_user $spotify_pass" - ts3

echo "setting up vnc"
screen -dmS x11vnc x11vnc -display :99 -forever -shared -ncache 10 -nopw

#connect to server after 10sec
sh /scripts/connect.sh $ts3_address &

su -c "sh /scripts/ts3start.sh" - ts3