
**[https://gitlab.com/cyberdos/ts3spotify](https://gitlab.com/cyberdos/ts3spotify)**

# Usage
This tool enables you to play your favorite spotify music on your Teamspeak3 server.

The bot joins on to the specified server and starts broadcasting automaticly.

![Screenshot](https://gitlab.com/cyberdos/ts3spotify/-/raw/master/docs/screen1.PNG)


## Server channel settings
These are the recommended Teamspeak3 channel settings:

![Screenshot](https://gitlab.com/cyberdos/ts3spotify/-/raw/master/docs/screen3.PNG)
## Spotify control
You can control the bot with your spotify client.

Just select the bot in the devices menu.

![Screenshot](https://gitlab.com/cyberdos/ts3spotify/-/raw/master/docs/screen4.PNG)

# Setup
This tool requires [docker](https://www.docker.com/)!

## 1. Docker Hub  (recommended)
Replace the variables with your own details.

    docker run -e "spotify_user=SPOTIFYUSERNAME" -e "spotify_pass=SPOTIFYPASSWORD" -e "ts3_address=TS3SERVERADDRESS" --restart unless-stopped -d cyberdos/ts3spotify:latest

## 2. Build it yourself
Replace the variables with your own details.

    git clone https://gitlab.com/cyberdos/ts3spotify.git
    docker build -t cyberdos/ts3spotify:diy .
    docker run -e "spotify_user=SPOTIFYUSERNAME" -e "spotify_pass=SPOTIFYPASSWORD" -e "ts3_address=TS3SERVERADDRESS" --restart unless-stopped -d ts3spotify:diy

# Debug
To debug the container, just expose port 5900/tcp and connect via VNC.

    docker run -p 5900:5900 -e "spotify_user=SPOTIFYUSERNAME" -e "spotify_pass=SPOTIFYPASSWORD" -e "ts3_address=TS3SERVERADDRESS" -it cyberdos/ts3spotify:latest
