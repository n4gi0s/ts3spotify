export DISPLAY=:99
#start teamspeak
screen -dmS ts3 sh /scripts/ts3start.sh
sleep 5 #wait for startup

#accept eula
xdotool key End
xdotool mousemove 580 680 click 1
sleep 1
#click on ad
xdotool mousemove 630 740 click 1
sleep 1
#not use myteamspeak
xdotool mousemove 100 330 click 1
sleep 3
#set username
xdotool type $1
xdotool mousemove 320 260 click 1

#stop teamspeak
screen -XS ts3 quit
sleep 3 #wait for shutdown

#select audio devices
sqlite3 /home/ts3/.ts3client/settings.db "INSERT OR REPLACE INTO Profiles VALUES(1588178401,'Capture/Default/PreProcessing',replace('voiceactivation_level=-50\necho_reduction_db=10\necho_cancellation=false\nagc=true\necho_reduction=false\ndenoise=false\nvad_mode=1\nvad=true\nvad_over_ptt=false\ndelay_ptt=true\ncontinous_transmission=false\ntyping_suppression=false\ndelay_ptt_msecs=300\ndenoiser_level=1','\n',char(10)));" "INSERT OR REPLACE INTO Profiles VALUES(1588172133,'Playback/Default',replace('PlayMicClicksOnOthers=false\nDevice=DummyOutput\nPlayMicClicksOnOwn=false\nMode=PulseAudio\nAGC=true\nComfortNoiseVolume=-60\nDeviceDisplayName=Virtual_Dummy_Output\nComfortNoiseEnabled=false\nVolumeModifier=0\nVolumeFactorWaveDb=-17\nMonoSoundExpansion=2','\n',char(10)));" "INSERT OR REPLACE INTO Profiles VALUES(1588172146,'Capture/Default',replace('Device=VirtualMic\nMode=PulseAudio\nDeviceDisplayName=Virtual Source VirtualMic on Monitor of Virtual_Dummy_Output','\n',char(10)));" ".exit"