#!/bin/sh
export DISPLAY=:99

#start spotify
screen -dmS spotify sh /scripts/spotify.sh
sleep 5 #wait for startup

xdotool mousemove 600 590 click 1
sleep 1
xdotool type $1
sleep 1
xdotool key Tab
sleep 1
xdotool type $2
sleep 1
xdotool key Return
sleep 5